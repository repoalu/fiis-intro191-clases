/*
 * menu.cpp
 *
 *  Created on: Jun 13, 2019
 *      Author: JC
 */


#include<iostream>
using namespace std;

#include "../../prob01/prob01.hpp"
#include "../../prob02/prob02.hpp"
#include "../../prob03/prob03.hpp"
#include "../../prob04/prob04.hpp"
#include "../parametros/parametros.hpp"


void mostrarMenu(int opcion, bool bucle = false){
	if(opcion == MENU_OPCION_SELECCIONAR){
		cout << "Digite el numero de problema o ingrese 0 para salir: ";
		cin >> opcion;
		mostrarMenu(opcion, bucle);
	}else{
		switch(opcion){
			case MENU_OPCION_SALIR: return;
			case 1: prob01(); break;
			case 2: prob02(); break;
			case 3: prob03(); break;
			case 4: prob04(); break;
			default:
				cout << "Opcion no valida" << endl;
		}
		if(bucle){
			mostrarMenu(MENU_OPCION_SELECCIONAR, bucle);
		}
	}

}

