/*
 * utilitarios.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include "parametros/parametros.hpp"
using namespace std;

void imprimir(int A[], int n){
	cout << "[ ";
	for(int i = 0; i < n; i++){
		cout << A[i] << " ";
	}
	cout << " ]" << endl;
}

void imprimir(string nombreVector, float *A, int tamano){
	cout << nombreVector << " = [ ";
	for(int i = 0; i < tamano; i++){
		cout << A[i] << " ";
	}
	cout << "] " << endl;

}

void imprimir(string nombreVector, int *A, int tamano){
	cout << nombreVector << " = [ ";
	for(int i = 0; i < tamano; i++){
		cout << A[i] << " ";
	}
	cout << "] " << endl;

}

void imprimir(int A[][MAX_COL], int f, int c){
	for(int i = 0; i < f; i++){
		for(int j = 0; j < c; j++){
			cout << A[i][j] << " ";
		}
		cout << endl;
	}
}

void imprimir(char A[][MAX_COL], int f, int c){
	for(int i = 0; i < f; i++){
		for(int j = 0; j < c; j++){
			cout << A[i][j] << " ";
		}
		cout << endl;
	}
}
