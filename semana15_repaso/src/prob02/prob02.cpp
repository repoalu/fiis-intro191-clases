/*
 * prob02.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
#include"../utilitarios/parametros/parametros.hpp"
using namespace std;

float promedioElementosDiag(int N[][MAX_COL], int orden);

void prob02(){

	int orden = 5;
	int matriz[][MAX_COL] = {	{2, 5, 7, 9, 0},
								{4, 8, 0, 1, 7},
								{3, 6, 1, 8, 0},
								{3, 2, 8, 1, 9},
								{4, 9, 9, 1, 4}
							};
	cout << "Entrada: " << endl;
	imprimir(matriz, orden, orden);
	float respuesta = promedioElementosDiag(matriz, orden);
	cout << "Promedio: " << respuesta << endl;
}


float promedioElementosDiag(int N[][MAX_COL], int orden){
	int suma = 0;
	int contador = 0;
	for(int i = 0; i < orden; i++){
		for(int j = 0; j < orden; j++){
			if(i > j && i > orden - j - 1){
				suma += N[i][j];
				contador++;
			}
		}
	}
	return 1.0 * suma / contador;
}


