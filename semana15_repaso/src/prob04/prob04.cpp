/*
 * prob04.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

void imprimirEspiral(int M[][MAX_COL], int orden);
void imprimirBorde(int matriz[][MAX_COL], int inicio, int longitud);

void prob04(){
	cout << "Prob04: " << endl;
	cin.get();

	int ordenTest = 5;
	int matriz[][MAX_COL] = {	{1 , 2 , 3 , 4 , 5 },
								{6 , 7 , 8 , 9 , 10},
								{11, 12, 13, 14, 15},
								{16, 17, 18, 19, 20},
								{21, 22, 23, 24, 25}

	};
	cout << "Matriz de entrada: " << endl;
	imprimir(matriz, ordenTest, ordenTest);
	cout << "Espiral: " << endl;
	imprimirEspiral(matriz, ordenTest);

}

void imprimirEspiral(int M[][MAX_COL], int orden){
	for(int i = 0; i < orden - 1; i++){
		imprimirBorde(M, i, orden - 2 * i);
	}
}


void imprimirBorde(int matriz[][MAX_COL], int inicio, int longitud){
	if(longitud == 1){
		cout << matriz[inicio][inicio];
	}else{
		//lado superior
		for(int j = inicio; j < inicio + longitud - 1; j++){
			cout << matriz[inicio][j] << " ";
		}

		//lado derecho
		for(int i = inicio; i < inicio + longitud - 1; i++){
			cout << matriz[i][inicio + longitud - 1] << " ";
		}

		//lado inferior
		for(int j = inicio + longitud - 1; j >= inicio; j--){
			cout << matriz[inicio + longitud - 1][j] << " ";
		}

		//lado izquierdo
		for(int i = inicio + longitud - 2; i > inicio ; i--){
			cout << matriz[i][inicio] << " ";
		}


	}

}


