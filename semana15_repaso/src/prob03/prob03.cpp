/*
 * prob03.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

string evaluarJuego(string comandos, char tablero[][MAX_COL], int tamano);

void prob03(){
	int tamanoTablero = 3;
	char tablero[][MAX_COL] = { "X00",
								"300",
								"200"
							  };

	string comandos = "DDAIADUIIDD";
	cout << "Tablero Inicial: " << endl;
	imprimir(tablero, tamanoTablero, tamanoTablero);
	cout << endl;
	cout<< "Resultado: " << evaluarJuego(comandos, tablero, tamanoTablero);

}

string evaluarJuego(string comandos, char tablero[][MAX_COL], int tamano){
	//Posicion del robot
	int i = 0;
	int j = 0;

	const string MSJ_VIVO = "Vivo";
	const string MSJ_MUERTO = "Muerto";

	for(int indice = 0; indice < comandos.size(); indice++){
		cout << "Comando: " << comandos[indice] << endl;
		if(comandos[indice] == 'U'){
			if(i > 0){
				switch(tablero[i - 1][j]){
					case '1':
						break;
					case '2':
						return MSJ_MUERTO;
					case '3':
						tablero[i][j] = '0';
						i = 0;
						j = 0;
						break;
					default:
						tablero[i][j] = '0';
						i--;

				}
				tablero[i][j] = 'X';
			}else{
				return MSJ_MUERTO;
			}

		}else if(comandos[indice] == 'A'){
			if(i < tamano - 1){
				switch(tablero[i + 1][j]){
					case '1':
						break;
					case '2':
						return MSJ_MUERTO;
					case '3':
						tablero[i][j] = '0';
						i = 0;
						j = 0;
						break;
					default:
						tablero[i][j] = '0';
						i++;
				}
				tablero[i][j] = 'X';
			}else{
				return MSJ_MUERTO;
			}

		}else if(comandos[indice] == 'D'){
			if(j < tamano - 1){
				switch(tablero[i][j + 1]){
					case '1':
						break;
					case '2':
						return MSJ_MUERTO;
					case '3':
						tablero[i][j] = '0';
						i = 0;
						j = 0;
						break;
					default:
						tablero[i][j] = '0';
						j++;
				}
				tablero[i][j] = 'X';
			}else{
				return MSJ_MUERTO;
			}

		}else if(comandos[indice] == 'I'){
			if(j > 0){
				switch(tablero[i][j - 1]){
					case '1':
						break;
					case '2':
						return MSJ_MUERTO;
					case '3':
						tablero[i][j] = '0';
						i = 0;
						j = 0;
						break;
					default:
						tablero[i][j] = '0';
						j--;
				}
				tablero[i][j] = 'X';
			}else{
				return MSJ_MUERTO;
			}
		}
		imprimir(tablero, tamano, tamano); cout << endl;
	}
	return MSJ_VIVO;
}






