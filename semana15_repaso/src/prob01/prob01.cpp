/*
 * prob01.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
#include"../utilitarios/parametros/parametros.hpp"
using namespace std;

int obtenerCantidadMineral(int N[][MAX_COL], int f, int c);


void prob01(){

	int filas = 4;
	int col = 4;
	int matriz [][MAX_COL] = 	{ {30, 80, 50, 10},
			  	  	  	  	  	  {50, 20, 90, 30},
								  {20, 30, 10, 90},
								  {20, 80, 80, 20}
								};

	cout << "Matriz de entrada: " << endl;
	imprimir(matriz, filas, col);
	int mineral = obtenerCantidadMineral(matriz, filas, col);

	cout << "Mineral: " << mineral << endl;

}

int obtenerCantidadMineral(int N[][MAX_COL], int f, int c){
	int max = 0;
	for(int i = 0; i < f; i++){
		for(int j = 0; j < c; j++){
			//Maxima suma por cada valor de la matriz
			int maxCell = 0;
			int suma = 0;

			//Izquierda
			if(i > 0){
				suma = N[i - 1][j];
				if(suma > maxCell){
					maxCell = suma;
				}
			}
			//Derecha
			if(i < f - 1){
				suma = N[i + 1][j];
				if(suma > maxCell){
					maxCell = suma;
				}
			}
			//Arriba
			if(j > 0){
				suma = N[i][j - 1];
				if(suma > maxCell){
					maxCell = suma;
				}
			}
			//Abajo
			if(j < c - 1){
				suma = N[i][j + 1];
				if(suma > maxCell){
					maxCell = suma;
				}
			}
			maxCell += N[i][j];
			if(maxCell > max){
				max = maxCell;
			}
		}
	}
	return max;
}
