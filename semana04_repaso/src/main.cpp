#include <iostream>
using namespace std;

void prob01(){
	double a1, b1, a2, b2, a3, b3;
	cout << "Ingrese vertices: ";
	cin >> a1 >> b1 >> a2 >> b2 >> a3 >> b3;
	double area =  0.5 * (a1 * b2 + b1 * a3 + a2 * b3 - a3 * b2 - b1 * a2 - a1 * b3);
	if (area < 0){
		area = -1 * area;
	}
	cout<< "El area es: " << area<<endl;

	//A1-A2
	double refLado1 = (b2 - b1) * (b2 - b1) + (a2 - a1) * (a2 - a1);
	//A2-A3
	double refLado2 = (b2 - b3) * (b2 - b3) + (a2 - a3) * (a2 - a3);
	//A1-A3
	double refLado3 = (b1 - b3) * (b1 - b3) + (a1 - a3) * (a1 - a3);

	if(refLado1 == refLado2 && refLado2 == refLado3){
		cout << "Triangulo equilatero";
	}else if(refLado1 == refLado2 || refLado2 == refLado3 || refLado1 == refLado3){
		cout << "Triangulo isosceles";
	}else{
		cout << "Triangulo escaleno";
	}

}

void prob02(){
	double salario;
	int dia, mes, anio;
	cout << "Ingrese salario: ";
	cin >> salario;
	cout << "Ingrese fecha de ingreso (dia, mes y anio): ";
	cin >> dia >> mes >> anio;

	float promedioEvaluaciones = 0;
	int sumaEvaluaciones = 0;
	int cantidadEvaluaciones = 0;
	int evaluacion = 0;
	int anioActual = 2019;

	//Fundadores
	if(anio == 2015 && dia == 1 && mes == 1){
		salario *= 1.3;
	}else{

		if(anio <= anioActual - 3){
			cout << "Ingrese evaluacion "<<anioActual-3 <<": ";
			cin >> evaluacion;
			sumaEvaluaciones += evaluacion;
			cantidadEvaluaciones++;
		}

		if(anio <= anioActual - 2){
			cout << "Ingrese evaluacion "<<anioActual - 2 <<": ";
			cin >> evaluacion;
			sumaEvaluaciones += evaluacion;
			cantidadEvaluaciones++;

		}

		if(anio <= anioActual - 1){
			cout << "Ingrese evaluacion "<<anioActual - 1 <<": ";
			cin >> evaluacion;
			sumaEvaluaciones += evaluacion;
			cantidadEvaluaciones++;
		}

		if (anio == anioActual){
			cout << "Ingrese evaluacion "<<anioActual<<": ";
			cin >> evaluacion;
			sumaEvaluaciones += evaluacion;
			cantidadEvaluaciones++;
		}

		promedioEvaluaciones = 1.0 * sumaEvaluaciones  / cantidadEvaluaciones;
	}

	if(promedioEvaluaciones > 15){
			salario *= 1.2;
	}
	cout << "Nuevo salario: " << salario;

}


void prob03(){

	double salario1, salario2, salario3;
	int fecha1, fecha2, fecha3;
	int dia, mes, anio;

	cout << "Ingrese salario del empleado 1: ";
	cin >> salario1;
	cout << "Ingrese fecha de ingreso (dia, mes y anio): ";
	cin >> dia >> mes >> anio;
	fecha1 = 10000 * anio + 100 * mes + dia;

	cout << "Ingrese salario del empleado 2: ";
	cin >> salario2;
	cout << "Ingrese fecha de ingreso (dia, mes y anio): ";
	cin >> dia >> mes >> anio;
	fecha2 = 10000 * anio + 100 * mes + dia;


	cout << "Ingrese salario del empleado 3: ";
	cin >> salario3;
	cout << "Ingrese fecha de ingreso (dia, mes y anio): ";
	cin >> dia >> mes >> anio;
	fecha3 = 10000 * anio + 100 * mes + dia;

	int auxFecha = 0;
	double auxSalario = 0;

	if(fecha1 > fecha2){
		//Intercambiar elementos (swap)
		auxFecha = fecha1;
		fecha1 = fecha2;
		fecha2 = auxFecha;

		auxSalario = salario1;
		salario1 = salario2;
		salario2 = auxSalario;

	}

	if(fecha1 > fecha3){
		auxFecha = fecha1;
		fecha1 = fecha3;
		fecha3 = auxFecha;

		auxSalario = salario1;
		salario1 = salario3;
		salario3 = auxSalario;
	}

	if(fecha2 > fecha3){
		auxFecha = fecha2;
		fecha2 = fecha3;
		fecha3 = auxFecha;

		auxSalario = salario2;
		salario2 = salario3;
		salario3 = auxSalario;
	}

	//Casos de empate
	if(fecha1 == fecha2 && salario1 < salario2){
		auxSalario = salario1;
		salario1 = salario2;
		salario2 = auxSalario;
	}

	if(fecha1 == fecha3 && salario1 < salario3){
		auxSalario = salario1;
		salario1 = salario3;
		salario3 = auxSalario;
	}

	if(fecha2 == fecha3 && salario2 < salario3){
		auxSalario = salario2;
		salario2 = salario3;
		salario3 = auxSalario;
	}

	cout << "Datos ordenados: "<<endl;
	cout << "Fecha: " << fecha1 << "/ Salario: "<< salario1<<endl;
	cout << "Fecha: " << fecha2 << "/ Salario: "<< salario2<<endl;
	cout << "Fecha: " << fecha3 << "/ Salario: "<< salario3<<endl;
}

int main() {

	//Descomentar para pruebas de cada problema

	//prob01();

	prob02();

	//prob03();

	return 0;
}

