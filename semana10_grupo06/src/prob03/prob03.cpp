/*
 * prob03.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

void calcularVuelto(float monto, float valores[], int cantidadMonedas[], int elementos){
	float resto = monto;
	int monedasEntregar[elementos];
	int contTipoMoneda = 0;

	for(int i = elementos - 1; i >= 0 && resto > 0; i--){
		if(valores[i] > 0){
			int monedas = resto / valores[i];
			if(monedas > cantidadMonedas[i]){
				monedas = cantidadMonedas[i];
			}
			monedasEntregar[i] = monedas;
			resto -= valores[i] * monedas;
			contTipoMoneda++;
		}else{
			monedasEntregar[i] = 0;
		}
	}
	cout << "Vuelto por Entregar: " << monto << endl;
	if(resto > 0){
		cout << "No es posible entregar el vuelto";
	}else{
		cout << "Detalle: " << endl;
		for(int i = elementos - 1; i >= elementos - contTipoMoneda; i--){
			cout << "Por el valor de " << valores[i] << ": " << monedasEntregar[i] << endl;
		}
	}
}

void prob03(){
	float valores[] = {1, 2, 5, 10, 20, 50, 100};
	int cantidades[] = {3, 50, 8, 12, 18, 25, 30};
	imprimir("valores", valores, sizeof(valores) / sizeof(int));
	imprimir("cantidades", cantidades, sizeof(cantidades) / sizeof(int));
	calcularVuelto(3853, valores, cantidades, sizeof(cantidades) / sizeof(int));
}

