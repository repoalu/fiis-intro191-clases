/*
 * prob04.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

void actualizarStockCartas(int codigosCaja[], int cantCajas, int cartasPorCaja[],
					       int cartasPorValor[], int cajasDescargar[], int cantCajasDescargar){
	for(int i = 0; i < cantCajasDescargar; i++){
		for(int j = 0; j < cantCajas; j++){
			if(cajasDescargar[i] == codigosCaja[j]){
				cout << cartasPorCaja[j] << endl;
				for(int k = 0; k < cartasPorCaja[j]; k++){
					cartasPorValor[k]++;
				}
			}
		}
	}
}

void prob04(){
	int CANT_VALORES_CARTAS = 13;
	int codigosCaja[] = {1031, 1158, 1258, 1355, 1498, 1568};
	int cartasPorCaja[] = {3, 8, 11, 5, 10, 8};
	int cantidadCartasInicial[] = {6, 5, 5, 4, 3, 8, 9, 11, 12, 14, 8, 9, 10};
	int cajasDescargar[] = {1031, 1568};
	int cantidadCajasDescargar = 2;

	imprimir("Cartas Inicio", cantidadCartasInicial, CANT_VALORES_CARTAS);

	actualizarStockCartas(codigosCaja, sizeof(codigosCaja) / sizeof(int),
						  cartasPorCaja, cantidadCartasInicial,
						  cajasDescargar, cantidadCajasDescargar);

	imprimir("Cartas Final", cantidadCartasInicial, CANT_VALORES_CARTAS);

}

