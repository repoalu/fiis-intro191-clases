/*
 * prob02.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

int calcularSuma(int digitos[], int cifras[]){
	int suma = 0;
	int indiceDigito = 0;
	int indiceCifra = 0;
	while(cifras[indiceCifra] != -1){
		int factor = 1;
		for(int i = 0; i < cifras[indiceCifra]; i++){
			suma += digitos[indiceDigito + cifras[indiceCifra] - i - 1] * factor;
			factor *= 10;
		}
		indiceDigito += cifras[indiceCifra];
		indiceCifra++;
	}
	return suma;
}

void prob02(){
	cout << "Prob 02" << endl;
	int digitos[] = {2, 2, 3, 4, 5, 3, 3, 5, 5, 5, -1};
	int cifras[] = {3, 4, 2, 1, -1};
	imprimir("digitos", digitos, sizeof(digitos) / sizeof(int));
	imprimir("cifras", cifras, sizeof(cifras) / sizeof(int));
	cout<< "Suma de elementos: " << calcularSuma(digitos, cifras);
}
