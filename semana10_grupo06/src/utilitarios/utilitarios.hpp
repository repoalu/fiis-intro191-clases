/*
 * utilitarios.hpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */

#ifndef UTILITARIOS_UTILITARIOS_HPP_
#define UTILITARIOS_UTILITARIOS_HPP_

#include<iostream>
using namespace std;

void imprimir(int A[], int n);
void imprimir(string nombreVector, int *A, int tamano);
void imprimir(string nombreVector, float *A, int tamano);

#endif /* UTILITARIOS_UTILITARIOS_HPP_ */
