/*
 * prob01.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

void mezclar(int M[], int elementosA, int N[], int elementosB, int mezcla[]){
	int idxA = 0;
	int idxB = 0;
	int seleccionado = 0;
	while(idxA < elementosA || idxB < elementosB){
		if(idxA >= elementosA){
			seleccionado = N[idxB];
			idxB++;
		}else if(idxB >= elementosB){
			seleccionado = M[idxA];
			idxA++;
		}else{
			if(N[idxB] < M[idxA]){
				seleccionado = N[idxB];
				idxB++;
			}else{
				seleccionado = M[idxA];
				idxA++;
			}
		}
		mezcla[idxA + idxB - 1] = seleccionado;
	}
}

void prob01(){
	int MAX_ELEM = 50;
	int A[] = {2, 3, 3, 4, 5, 6, 6, 22, 56, 67};
	int tamanoA = sizeof(A) / sizeof(int);
	int B[] = {3, 5, 7, 11};
	int tamanoB = sizeof(B) / sizeof(int);
	int C[MAX_ELEM];
	mezclar(A, tamanoA, B, tamanoB, C);
	imprimir("A", A, tamanoA);
	imprimir("B", B, tamanoB);
	imprimir("Resultado", C, tamanoA + tamanoB);
}



