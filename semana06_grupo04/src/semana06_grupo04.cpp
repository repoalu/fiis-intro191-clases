//============================================================================
// Name        : semana06_grupo04.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <iomanip>

using namespace std;

void prob01(){
	for(int i = 1; 2 * i < 200; i++){
		cout << 2 * i << " ";
	}
}

void prob02(){
	for(int i = 1; i <= 1000; i++){
		if(i % 7 == 0 && i % 5 != 0){
			cout << i << " ";
		}
	}

}

void prob03(){
	int contador = 0;
	for(int i = -1; i >= -700; i--){
		if(i % 7 == 0 && i % 2 != 0){
			cout << i << " ";
			contador++;
			if(contador % 10 == 0){
				cout << endl;
			}
		}
	}
}

void prob04_a(){
	int numero = 73664789;
	int mayor = numero % 10;
	int menor = numero % 10;

	while(numero != 0){
		int digito = numero % 10;
		if(digito < menor){
			menor = digito;
		}
		if(digito > mayor){
			mayor = digito;
		}
		numero /= 10;
	}
	cout << "Digito mayor: " << mayor << endl;
	cout << "Digito menor: " << menor << endl;
}

void prob04_b(){
	int N = 101101010;
	int k = 2;
	bool valido = true;

	int copia = N;

	while (copia != 0 && valido){
		int digito = copia % 10;
		if(digito >= k){
			valido = false;
		}
		copia /= 10;
	}
	if(valido){
		cout << "Numero " << N << " es valido en base " << k;
	}else{
		cout << "Numero " << N << " no es valido en base " << k;
	}
}


void prob04_c(){
	int N = 18;
	int k = 7;

	int termino = 0;

	for(int i = 1; i <= N ; i++){
		if(i % k == 0){
			termino += (10 - k + 1);
		}else{
			termino++;
		}
		cout << termino << " ";
	}
}

void prob09(){
	//Dato de entrada
	int k = 6;

	int primero = 0, segundo = 0, tercero = 1 , numero =0;
	int suma = primero + segundo + tercero;

	for(int i = 2; i <= k; i++){
		numero = primero + segundo + tercero;
		suma += numero;
		primero = segundo;
		segundo = tercero;
		tercero = numero;
	}

	cout << "Valor de S: "<< suma << endl;
}

void prob10(){
	//Dato de entrada
	int N = 8128;
	int sumaDivProp = 0;

	for(int i = 1; i < N; i++){
		if(i * i == N){
			cout << "1" << endl;
		}
		if(i * i * i == N){
			cout << "2" << endl;
		}
		if(N % i == 0){
			sumaDivProp += i;
		}
	}
	if(sumaDivProp == N){
		cout << "3";
	}
}


void prob11(){
	//Dato de entrada
	int N = 455869112;

	int digito = 0, anterior = 0, anterior2 = 0;
	int resultado = 0, factor = 1;

	while(N / 100 > 0){
		digito = N % 10;
		anterior = (N / 10) % 10;
		anterior2 = (N / 100) % 10;

		if(digito > anterior && digito > anterior2){
			resultado += digito * factor;
			factor *= 10;
		}
		N /= 10;
	}
	cout << resultado <<endl;
}

void prob12(){

	//Datos de entrada
	long votosCandidato1 = 458568;
	long votosCandidato2 = 128558;
	long votosCandidato3 = 237568;

	int TOTAL_SEGMENTOS = 20;
	long totalVotos = votosCandidato1 + votosCandidato2 + votosCandidato3;

	//Calculamos porcentaje por candidatos
	double porcentaje1 = 1.0 * votosCandidato1 / totalVotos * 100;
	double porcentaje2 = 1.0 * votosCandidato2 / totalVotos * 100;
	double porcentaje3 = 1.0 * votosCandidato3 / totalVotos * 100;

	//Calculamos cantidad de segmentos (o "barritas" representadas con asteriscos) que veremos
	int segmentos1 = porcentaje1 *  TOTAL_SEGMENTOS / 100;
	int segmentos2 = porcentaje2 *  TOTAL_SEGMENTOS / 100;
	int segmentos3 = porcentaje3 *  TOTAL_SEGMENTOS / 100;

	cout<<porcentaje1;

	//Distinguimos entre un segmento de una barra y uno en blanco
	string segmentoBarra = "    **   ";
	string segmentoVacio = "         ";

	for(int i = TOTAL_SEGMENTOS + 1; i > 0 ; i--){
		if(i == segmentos1 + 1){
			cout << fixed << setprecision(2) << "  "<< porcentaje1 << "  ";
		}else if(i <= segmentos1){
			cout << segmentoBarra;
		}else{
			cout << segmentoVacio;
		}

		if(i == segmentos2 + 1){
			cout << fixed << setprecision(2) << "  "<< porcentaje2 << "  ";
		}else if(i <= segmentos2){
			cout << segmentoBarra;
		}else{
			cout << segmentoVacio;
		}

		if(i == segmentos3 + 1){
			cout << fixed << setprecision(2) << "  "<< porcentaje3 << "  ";
		}else if(i <= segmentos3){
			cout << segmentoBarra;
		}else{
			cout << segmentoVacio;
		}
		cout << endl;
	}
	cout << "  Cand1  " << "  Cand2  " << "  Cand3  ";

}

int main() {
	//prob01();
	//prob02();
	//prob03();
	//prob04_a();
	//prob04_b();
	//prob04_c();
	//prob09();
	//prob10();
	//prob11();
	prob12();
}
